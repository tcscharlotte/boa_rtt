
    Morris.Donut({
        element: 'morris-donut-chart1',
        data: [{
            label: "Zip Code",
            value: 1
        }, {
            label: "Name",
            value: 1
        }, {
            label: "Email",
            value: 1
        }],
        resize: true
    });
    
    Morris.Donut({
        element: 'morris-donut-chart2',
        data: [{
            label: "Automated",
            value: 2
        }, {
            label: "Manual",
            value: 1
            
        }],
        resize: true

    });


    Morris.Donut({
        element: 'morris-donut-chart3',
        data: [{
            label: "Active",
            value: 1
        }, {
            label: "Closed",
            value: 2
        }],
        resize: true
    });
    
    Morris.Donut({
        element: 'morris-donut-chart4',
        data: [
//            {
//            label: "A : 1 (OPENED) \n ",
//            value: 0
//        }, {
//            label: "A : 2 (SENT TO QACC) \n ",
//            value: 0
//        }, {
//            label: "A : 3 (SENT FOR \n CLOSURE CONFIRMATION) \n ",
//            value: 0
//        }, {
//            label: "A : 4 (READY FOR \n REMEDIATION) \n ",
//            value: 0
//        }, {
//            label: "B : 5 (SENT FOR \n CLOSURE) \n ",
//            value: 0
//        }, 
            {
            label: "C : 6 (PENDING \n REMEDIATION) \n ",
            value: 1
        }, 
//            {
//            label: "C : 7 (PENDING \n VERIFICATION) \n ",
//            value: 0
//        }, {
//            label: "D : 8 (PENDING \n DEFECT ANALYSIS) \n ",
//            value: 0
//        }, {
//            label: "D : 9 (MANUAL SCRIPT \n READY) \n ",
//            value: 0
//        }, {
//            label: "D : 10 (PENDING \n PRIORITIZATION) \n ",
//            value: 0
//        }, {
//            label: "E : 11 (BATCH RECEIVED) \n ",
//            value: 0
//        }, {
//            label: "E : 12 (MANUAL REM. \n IN PROGRESS) \n ",
//            value: 0
//        }, {
//            label: "E : 13 (UPDATES READY) \n ",
//            value: 0
//        }, {
//            label: "E : 14 (LEO UPDATES \n APPLIED) \n ",
//            value: 0
//        }, {
//            label: "F : 15 (PENDING \n SCORECARD) \n ",
//            value: 0
//        }, {
//            label: "F : 16 (REMEDIATION \n FAILED) \n ",
//            value: 0
//        }, {
//            label: "G : 17 (SCORECARD \n PASSED) \n ",
//            value: 0
//        }, 
            {
            label: "H : 18 (CLOSED) \n ",
            value: 2
        }
              ],
        resize: true
    });