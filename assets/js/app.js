function confirm(){


bootbox.dialog({
  message: "Does this GCI meet the defined closure criteria? <br><br> <span class=\"text-danger\">Note: If all GCIs in this batch meet closure criteria, the batch will be sent to LOB for final closure analysis.</span>",
  title: "Closure Criteria",
  buttons: {
    success: {
      label: "Yes",
      className: "btn-success",
      callback: function() {
          window.location="GCIListQACC1.3.html";
        Example.show("");
      }
    },
    danger: {
      label: "No",
      className: "btn-danger",
      callback: function() {
           window.location="GCIListQACC1.2.html";
        Example.show("");
      }
    }
  }
}); 


}

function confirm2(){


bootbox.dialog({
  message: "Does this GCI meet the defined closure criteria? <br><br> <span class=\"text-danger\">Note: If all GCIs in this batch meet closure criteria, the batch will be sent to LOB for final closure analysis.</span>",
  title: "Closure Criteria",
  buttons: {
    success: {
      label: "Yes",
      className: "btn-success",
      callback: function() {
          window.location="GCIListQACC3.1.html";
        Example.show("");
      }
    },
    danger: {
      label: "No",
      className: "btn-danger",
      callback: function() {
           window.location="CustomerQACC4.html";
        Example.show("");
      }
    }
  }
}); 


}


function closureConfirm(){


bootbox.dialog({
  message: "Are you sure you want to confirm closure? <br><br> <span class=\"text-danger\">Note: If you click \"Yes\", This batch will be sent to the closure team for final closure. By clicking \"No\", you will send this batch back to QACC for remediation analysis. </span>",
  title: "Closure Criteria",
  buttons: {
    success: {
      label: "Yes",
      className: "btn-success",
      callback: function() {
          window.location="dashboard-lob2.html";
        Example.show("");
      }
    },
    danger: {
      label: "No",
      className: "btn-danger",
      callback: function() {
           window.location="dashboard-lob2.html";
        Example.show("");
      }
    },
      default: {
      label: "Cancel",
      className: "btn-default",
      callback: function() {
           window.location="GCIListLOB.html";
        Example.show("");
      }
    }
  }
}); 


}

function sendClosureCriteria(){
    
    bootbox.confirm("You are about to send this batch to the LOB team for closure confirmation - are you sure?", function(result) {
       
        if (result){
             window.location="dashboard-qacc.html";
        }
        else{
             window.location="GCIListQACC1.3.html";
        }
  Example.show("Confirm result: "+result);
}); 
}

function sendClosureCriteria2(){
    
    bootbox.confirm("You are about to send this batch to the LOB team for closure confirmation - are you sure?", function(result) {
       
        if (result){
             window.location="dashboard-qacc3.html";
        }
        else{
             window.location="GCIListQACC3.1.html";
        }
  Example.show("Confirm result: "+result);
}); 
}

function sendToRemediation(){
    
    bootbox.confirm("You are about to submit this batch for defect remediation - are you sure? <br><br> <span class=\"text-danger\">Note: If submitted, this batch will appear under the <strong> \"Pending Remediation Analysis\"</strong> tab on the QACC Dashboard.</span>", function(result) {
       
        if (result){
             window.location="dashboard-qacc2.html";
        }
        else{
             window.location="GCIListQACC1.2.html";
        }
  Example.show("Confirm result: "+result);
}); 
}

function sendToAuto(){
    
    bootbox.confirm("Are you sure you want to send this defect to the Automated Remediation team?", function(result) {
       
        if (result){
             window.location="GCIListQACC2.1.html";
        }
        else{
             window.location="customerQACC2.html";
        }
  Example.show("Confirm result: "+result);
}); 
}

function runManual(){
    
    bootbox.confirm("Are you sure you would like to submit your change to the Zip Code?", function(result) {
       
        if (result){
             window.location="customerManual2.html";
        }
        else{
             window.location="customerManual.html";
        }
  Example.show("Confirm result: "+result);
}); 
}

function runUpdates(){
    
    bootbox.confirm("Are you sure you would like to submit all of these remediation updates? <br><br> Changes were made to: <b>Zip Code</b>", function(result) {
       
        if (result){
             window.location="customerManual3.html";
        }
        else{
             window.location="customerManual2.html";
        }
  Example.show("Confirm result: "+result);
}); 
}

function applyToLEO(){
    
    bootbox.confirm("Are you sure you would like to push these changes into LEO? <br><br> Changes were made to: <b>Zip Code</b>", function(result) {
       
        if (result){
             window.location="dashboard-manual2.html";
        }
        else{
             window.location="customerManual3.html";
        }
  Example.show("Confirm result: "+result);
}); 
}

function sendToManual(){
    
    bootbox.confirm("Are you sure you want to send this defect to the Manual Remediation team?", function(result) {
       
        if (result){
             window.location="GCIListQACC2.1.html";
        }
        else{
             window.location="customerQACC2.html";
        }
  Example.show("Confirm result: "+result);
}); 
}

function sendToManual2(){
    
    bootbox.confirm("Are you sure you want to send this defect to the Manual Remediation team?", function(result) {
       
        if (result){
             window.location="GCIListQACC2.2.html";
        }
        else{
             window.location="customerQACC2.html";
        }
  Example.show("Confirm result: "+result);
}); 
}

function runAutoRem(){
    
    bootbox.confirm("Are you sure you want to run the automated remediation script for the selected defect(s)? <br><br> <span class=\"text-danger\">Note: If submitted, this defect will appear under the <strong> \"Pending Verification\"</strong> tab on the Automated Dashboard after the changes have been applied.</span>", function(result) {
       
        if (result){
             window.location="dashboard-automated2.html";
        }
        else{
             window.location="customerAutomated.html";
        }
  Example.show("Confirm result: "+result);
}); 
}

function verify(){
    
    bootbox.alert("Defect change verified. <br><br> <span class=\"text-danger\">Note: This defect and its change will now appear under the <strong> \"Pending Scorecard\"</strong> tab on the Automated Dashboard.</span>", function(result) {
       
        window.location="dashboard-automated3.html";
        
}); 
}

function runScorecard(){
    
    bootbox.alert("Defect sent to Scorecard successfully. <br><br> <strong>Result: <span class=\"text-success\"?>PASS</span> or <span class=\"text-danger\"?>FAIL</strong> <br><br> For this demo, this defect will <span class=\"text-success\">PASS</span> and go to the Closure Team.", function(result) {

        window.location="dashboard-automated4.html";
}); 
}

function runScorecard2(){
    
    bootbox.alert("Defect sent to Scorecard successfully. <br><br> <strong>Result: <span class=\"text-success\"?>PASS</span> or <span class=\"text-danger\"?>FAIL</span></strong> <br><br> For this demo, this defect will <span class=\"text-success\">PASS</span> and go to the Closure Team.", function(result) {

        window.location="dashboard-manual3.html";
}); 
}

function login(){
    
    
    var username = document.getElementById('username').value;
    var password = document.getElementById('password').value;
    if (username == null || username == "") {
        alert("Username must be filled out");
        return false;
    }
    else if (password == null || password == ""){
         alert("Password must be filled out");
        return false;
    }
    else{
        
        var userRole = document.getElementById('userRole').value;
    
    console.log("User Role: "+userRole)
    if(userRole == "QACC"){
        window.location="dashboard-qacc.html";
    }
    else if (userRole == "LOB"){
        window.location="dashboard-lob.html";
    }
     else if (userRole == "Closure Team"){
         window.location="dashboard-ct.html";
    }
     else if (userRole == "Manual Team"){
         window.location="dashboard-manual.html";
    }
     else if (userRole == "Automated Team"){
         window.location="dashboard-automated.html";
    }
    else if (userRole == "Administrator"){
        window.location="dashboard-admin.html";
    }
        
        
        
    }
}


        